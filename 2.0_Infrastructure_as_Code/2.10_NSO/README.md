### Cisco Network Services Orchestrator (NSO)

Cisco Network Services Orchestrator is a Linux application which orchestrates the configuration life cycle of physical and virtual network devices.

NSO gathers, parses, and stores the configuration state of the network devices it manages in a configuration database (CDB). Users and applications can then "ask" NSO to create, read, update, or delete configuration in a programmatic way either ad hoc or through customizable network services.

NSO uses software packages called Network Element Drivers (NEDs) to facilitate telnet, SSH, or API interactions with the devices that it manages. The NED provides an abstraction layer that reads in the device's running configuration and parses it into a data-model-validated snapshot in the CDB.

The NEDs also allow for the reverse, creating network configuration from CDB data inputs and then sending the configurations to the network devices. There are hundreds of NEDs covering all the Cisco platforms (including IOS-XE, IOS-XR, NX-OS, and ASA) and all major non-Cisco platforms as well.

#### Cisco NSO - Architecture

![](nso-arch.png )

*__Reference__: https://developer.cisco.com/learningcenter/labs/learn-nso-the-easy-way/step-1-introduction/*

As per the Cisco Certified DevNet Expert (v1.0) Equipment and Software List, Cisco NSO and NEDS version.

* Cisco Network Services Orchestrator (NSO) local installation 5.5
* Cisco NSO IOS CLI NED 6.69
* Cisco NSO NX-OS CLI NED 5.21

#### Cisco NSO DevNet Expert (v1.0) Exam Topics

```Bash
2.10 Create a basic Cisco NSO service package to meet given business and technical requirements. The service would generate a network configuration on the target device platforms using the "cisco-ios-cli" NED and be of type "python-and-template"
2.10.a Create a service template from a provided NSO device configuration
2.10.b Create a basic YANG module for the service containers (including lists, leaf lists, data types, leaf references, and single argument "when" and "must" conditions)
2.10.c Create basic actions to verify operational status of the service
2.10.d Monitor service status by reviewing the NCS Python VM log file
```

### Cisco NSO Learning Links 

Cisco Network Services Orchestrator Solution Overview

https://www.cisco.com/c/en/us/solutions/service-provider/solutions-cloud-providers/network-services-orchestrator-solutions/solution-overview.html

Cisco Network Services Orchestrator (NSO) Dev Center

https://developer.cisco.com/site/nso/

NSO Fundamentals

https://developer.cisco.com/docs/nso/#!nso-fundamentals/nso-fundamentals

NSO Guides

https://developer.cisco.com/docs/nso/guides/#!nso-5-6-getting-started-guide-nso-introduction/nso-introduction

NSO Service Design

https://developer.cisco.com/docs/nso/#!service-design/basics

Introduction to Network Services Orchestrator - the single API and CLI for your network

https://pubhub.devnetcloud.com/media/netdevops-live/site/files/s01t06.pdf

Cisco Live - LABSPG-2442

https://www.ciscolive.com/c/dam/r/ciscolive/emea/docs/2019/pdf/LABSPG-2442-LG.pdf

Automating Service Discovery in NSO

https://www.ciscolive.com/c/dam/r/ciscolive/emea/docs/2020/pdf/BRKOPS-3339.pdf

### Cisco NSO Learning Labs 

Learning Labs Center

https://developer.cisco.com/learningcenter/search/labs/categories/Networking/products/NSO/?page=1

Get Started with Network Services Orchestrator

https://developer.cisco.com/learning/tracks/get_started_with_nso

